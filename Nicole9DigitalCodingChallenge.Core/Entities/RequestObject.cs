﻿using System.Collections.Generic;

namespace Nicole9DigitalCodingChallenge.Core.Entities
{
    public class RequestObject
    {
        public List<Payload> Payload { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public int TotalRecords { get; set; }
    }
}
