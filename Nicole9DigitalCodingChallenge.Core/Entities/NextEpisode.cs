﻿namespace Nicole9DigitalCodingChallenge.Core.Entities
{
    public class NextEpisode
    {
        public string Channel { get; set; }
        public string ChannelLogo { get; set; }
        public string Date { get; set; }
        public string Html { get; set; }
        public string Url { get; set; }
    }
}
