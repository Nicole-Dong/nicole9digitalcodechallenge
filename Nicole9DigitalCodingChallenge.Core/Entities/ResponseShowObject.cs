﻿namespace Nicole9DigitalCodingChallenge.Core.Entities
{
    public class ResponseShowObject
    {
        public string Image { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
    }
}
