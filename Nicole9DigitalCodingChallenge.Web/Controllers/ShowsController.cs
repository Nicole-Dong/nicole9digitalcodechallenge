﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Nicole9DigitalCodingChallenge.Core.Entities;

namespace Nicole9DigitalCodingChallenge.Web.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShowsController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Post([FromBody]RequestObject request)
        {
            try
            {
                if (!ModelState.IsValid || request == null || request?.Payload == null)
                    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest, 
                        new { error = "Could not decode request: JSON parsing failed" }));

                var result = request?.Payload
                   .Where(x => x.Drm && x.EpisodeCount > 0)
                   .Select(x => new ResponseShowObject { Image = x.Image?.ShowImage, Slug = x.Slug, Title = x.Title })
                   .ToList();

                return Ok(new ResponseOject { Response = result });
            }
            catch (Exception ex)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest, 
                    new { error = $"Could not decode request: {ex.Message}" }));
            }
        }
    }
}
