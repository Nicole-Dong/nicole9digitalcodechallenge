﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nicole9DigitalCodingChallenge.Core.Entities;
using Nicole9DigitalCodingChallenge.Web.Controllers;

namespace Nicole9DigitalCodingChallenge.Test
{
    [TestClass]
    public class TestShowsController
    {
        [TestMethod]
        public void Post_ShouldReturnCorrectShows()
        {
            var testRequest = GetTestPayLoads();
            var controller = new ShowsController();

            var count = testRequest.Payload.Count(x => x.Drm && x.EpisodeCount > 0);

            var result = controller.Post(testRequest) as OkNegotiatedContentResult<ResponseOject>;
            Assert.IsNotNull(result);
            Assert.AreEqual(count, result?.Content.Response.Count);
        }

        private RequestObject GetTestPayLoads()
        {
            var testPayLoads = new List<Payload>
            {
                new Payload()
                {
                    Country = "UK",
                    Description = "What's life like when you have enough children to field your own football team?",
                    Drm = true,
                    EpisodeCount = 3,
                    Genre = "Reality",
                    Image =
                        new Image()
                        {
                            ShowImage = "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg"
                        },
                    Language = "English",
                    NextEpisode = null,
                    PrimaryColour = "#ff7800",
                    Seasons = new List<Season>() {new Season {Slug = "show/16kidsandcounting/season/1"}},
                    Slug = "show/16kidsandcounting",
                    Title = "16 Kids and Counting",
                    TvChannel = "GEM"
                },
                new Payload()
                {
                    Slug = "show/seapatrol",
                    Title = "Sea Patrol",
                    TvChannel = "Channel 9"
                },
                new Payload()
                {
                    Country = "USA",
                    Description =
                        "The Taste puts 16 culinary competitors in the kitchen, where four of the World's most notable culinary masters of the food world judges their creations based on a blind taste. Join judges Anthony Bourdain, Nigella Lawson, Ludovic Lefebvre and Brian Malarkey in this pressure-packed contest where a single spoonful can catapult a contender to the top or send them packing.",
                    Drm = true,
                    EpisodeCount = 2,
                    Genre = "Reality",
                    Image = new Image() {ShowImage = "http://mybeautifulcatchupservice.com/img/shows/TheTaste1280.jpg"},
                    Language = "English",
                    NextEpisode = new NextEpisode()
                    {
                        Channel = null,
                        ChannelLogo = "http://mybeautifulcatchupservice.com/img/player/logo_go.gif",
                        Date = null,
                        Html = "<br><span class=\"visit\">Visit the Official Website</span></span>",
                        Url = "http://go.ninemsn.com.au/"
                    },
                    PrimaryColour = "#df0000",
                    Seasons = new List<Season>() {new Season {Slug = "show/thetaste/season/1"}},
                    Slug = "show/thetaste",
                    Title = "The Taste",
                    TvChannel = "GEM"
                },
                new Payload()
                {
                    Country = "UK",
                    Description =
                        "The series follows the adventures of International Rescue, an organisation created to help those in grave danger using technically advanced equipment and machinery. The series focuses on the head of the organisation, ex-astronaut Jeff Tracy, and his five sons who piloted the \"Thunderbird\" machines.",
                    Drm = true,
                    EpisodeCount = 24,
                    Genre = "Action",
                    Image =
                        new Image() {ShowImage = "http://mybeautifulcatchupservice.com/img/shows/Thunderbirds_1280.jpg"},
                    Language = "English",
                    NextEpisode = null,
                    PrimaryColour = "#0084da",
                    Seasons = new List<Season>()
                    {
                        new Season {Slug = "show/thunderbirds/season/1"},
                        new Season {Slug = "show/thunderbirds/season/3"},
                        new Season {Slug = "show/thunderbirds/season/4"},
                        new Season {Slug = "show/thunderbirds/season/5"},
                        new Season {Slug = "show/thunderbirds/season/6"},
                        new Season {Slug = "show/thunderbirds/season/8"}
                    },
                    Slug = "show/thunderbirds",
                    Title = "Thunderbirds",
                    TvChannel = "Channel 9"
                },
                new Payload()
                {
                    Country = "USA",
                    Description =
                        "A sleepy little village, Crystal Cove boasts a long history of ghost sightings, poltergeists, demon possessions, phantoms and other paranormal occurrences. The renowned sleuthing team of Fred, Daphne, Velma, Shaggy and Scooby-Doo prove all of this simply isn't real, and along the way, uncover a larger, season-long mystery that will change everything.",
                    Drm = true,
                    EpisodeCount = 4,
                    Genre = "Kids",
                    Image = new Image() {ShowImage = "http://mybeautifulcatchupservice.com/img/shows/ScoobyDoo1280.jpg"},
                    Language = "English",
                    NextEpisode = null,
                    PrimaryColour = "#1b9e00",
                    Seasons = new List<Season>() {new Season {Slug = "show/scoobydoomysteryincorporated/season/1"}},
                    Slug = "show/scoobydoomysteryincorporated",
                    Title = "Scooby-Doo! Mystery Incorporated",
                    TvChannel = "GO!"
                },
                new Payload()
                {
                    Country = "USA",
                    Description =
                        "Toy Hunter follows toy and collectibles expert and dealer Jordan Hembrough as he scours the U.S. for hidden treasures to sell to buyers around the world. In each episode, he travels from city to city, strategically manoeuvring around reluctant sellers, abating budgets, and avoiding unforeseen roadblocks.",
                    Drm = true,
                    EpisodeCount = 2,
                    Genre = "Reality",
                    Image = new Image() {ShowImage = "http://mybeautifulcatchupservice.com/img/shows/ScoobyDoo1280.jpg"},
                    Language = "English",
                    NextEpisode = null,
                    PrimaryColour = "#0084da",
                    Seasons = new List<Season>() {new Season {Slug = "show/toyhunter/season/1"}},
                    Slug = "show/toyhunter",
                    Title = "Toy Hunter",
                    TvChannel = "GO!"
                },
                new Payload()
                {
                    Country = "AUS",
                    Description =
                        "A series of documentary specials featuring some of the world's most frightening moments, greatest daredevils and craziest weddings.",
                    Drm = true,
                    EpisodeCount = 1,
                    Genre = "Documentary",
                    Image = new Image() {ShowImage = "http://mybeautifulcatchupservice.com/img/shows/Worlds1280.jpg"},
                    Language = "English",
                    NextEpisode = null,
                    PrimaryColour = "#ff7800",
                    Seasons = new List<Season>() {new Season {Slug = "show/worlds/season/1"}},
                    Slug = "show/worlds",
                    Title = "World's...",
                    TvChannel = "Channel 9"
                },
                new Payload()
                {
                    Country = "USA",
                    Description =
                        "Another year of bachelorhood brought many new adventures for roommates Walden Schmidt and Alan Harper. After his girlfriend turned down his marriage proposal, Walden was thrown back into the dating world in a serious way. The guys may have thought things were going to slow down once Jake got transferred to Japan, but they're about to be proven wrong when a niece of Alan's, who shares more than a few characteristics with her father, shows up at the beach house.",
                    Drm = true,
                    EpisodeCount = 0,
                    Genre = "Comedy",
                    Image =
                        new Image()
                        {
                            ShowImage = "http://mybeautifulcatchupservice.com/img/shows/TwoandahHalfMen_V2.jpg"
                        },
                    Language = "English",
                    NextEpisode = new NextEpisode()
                    {
                        Channel = null,
                        ChannelLogo = "http://mybeautifulcatchupservice.com/img/player/Ch9_new_logo.gif",
                        Date = null,
                        Html =
                            "Next episode airs: <span> 10:00pm Monday on<br><span class=\"visit\">Visit the Official Website</span></span>",
                        Url = "http://channelnine.ninemsn.com.au/twoandahalfmen/"
                    },
                    PrimaryColour = "#ff7800",
                    Seasons = null,
                    Slug = "show/twoandahalfmen",
                    Title = "Two and a Half Men",
                    TvChannel = "Channel 9"
                },
                new Payload()
                {
                    Country = "USA",
                    Description =
                        "Simmering with supernatural elements and featuring familiar and fan-favourite characters from the immensely popular drama The Vampire Diaries, it's The Originals. This sexy new series centres on the Original vampire family and the dangerous vampire/werewolf hybrid, Klaus, who returns to the magical melting pot that is the French Quarter of New Orleans, a town he helped build centuries ago.",
                    Drm = true,
                    EpisodeCount = 1,
                    Genre = "Action",
                    Image =
                        new Image()
                        {
                            ShowImage = "http://mybeautifulcatchupservice.com/img/shows/TwoandahHalfMen_V2.jpg"
                        },
                    Language = "English",
                    NextEpisode = new NextEpisode()
                    {
                        Channel = null,
                        ChannelLogo = "http://mybeautifulcatchupservice.com/img/player/logo_go.gif",
                        Date = null,
                        Html = "<br><span class=\"visit\">Visit the Official Website</span></span>",
                        Url = "http://go.ninemsn.com.au/"
                    },
                    PrimaryColour = "#df0000",
                    Seasons = new List<Season>() {new Season {Slug = "show/theoriginals/season/1"}},
                    Slug = "show/theoriginals",
                    Title = "The Originals",
                    TvChannel = "GO!"
                },
                new Payload()
                {
                    Country = "AUS",
                    Description =
                        "Join the most dynamic TV judging panel Australia has ever seen as they uncover the next breed of superstars every Sunday night. UK comedy royalty Dawn French, international pop superstar Geri Halliwell, (in) famous Aussie straight-talking radio jock Kyle Sandilands, and chart -topping former AGT alumni Timomatic.",
                    Drm = false,
                    EpisodeCount = 0,
                    Genre = "Reality",
                    Image = new Image() {ShowImage = "http://mybeautifulcatchupservice.com/img/shows/AGT.jpg"},
                    Language = "English",
                    NextEpisode = new NextEpisode()
                    {
                        Channel = null,
                        ChannelLogo = "http://mybeautifulcatchupservice.com/img/player/Ch9_new_logo.gif",
                        Date = null,
                        Html =
                            "Next episode airs:<span>6:30pm Sunday on<br><span class=\"visit\">Visit the Official Website</span></span>",
                        Url = "http://agt.ninemsn.com.au"
                    },
                    PrimaryColour = "#df0000",
                    Seasons = new List<Season>() {new Season {Slug = "show/theoriginals/season/1"}},
                    Slug = "show/australiasgottalent",
                    Title = "Australia's Got Talent",
                    TvChannel = "Channel 9"
                }
            };

            var mockRequest = new RequestObject()
            {
                Payload = testPayLoads,
                Skip = 0,
                Take = 10,
                TotalRecords = 10
            };

            return mockRequest;
        }
    }
}
