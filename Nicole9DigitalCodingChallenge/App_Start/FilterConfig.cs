﻿using System.Web;
using System.Web.Mvc;

namespace Nicole9DigitalCodingChallenge
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
